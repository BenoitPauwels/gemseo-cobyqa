# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# Copyright 2022 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Contributors:
#    INITIAL AUTHORS - API and implementation and/or documentation
#        :author: Francois Gallard
#    OTHER AUTHORS   - MACROSCOPIC CHANGES
from math import cos
from math import sin

import pytest
from gemseo import configure_logger
from gemseo import execute_algo
from gemseo.algos.design_space import DesignSpace
from gemseo.algos.opt_problem import OptimizationProblem
from gemseo.core.mdofunctions.mdo_function import MDOFunction
from gemseo.problems.analytical.power_2 import Power2

configure_logger()


@pytest.fixture()
def problem() -> Power2:
    """The Power2 optimization problem."""
    return Power2()


def test_cobyqa_power2(problem):
    """Test gemseo interface on the Power2 problem."""
    result = execute_algo(opt_problem=problem, algo_name="COBYQA")
    assert abs(problem.get_solution()[1] - result.f_opt) < 1e-3


def test_scalar_ineq():
    """Test gemseo interface on a problem with scalar values constraints."""
    design_space = DesignSpace()
    design_space.add_variable("x", l_b=0, u_b=1, value=0.5)
    problem = OptimizationProblem(design_space)
    problem.objective = MDOFunction(sin, "sin")
    problem.add_constraint(MDOFunction(cos, "cos"), cstr_type="eq", value=0.5)
    problem.add_constraint(MDOFunction(cos, "cos"), cstr_type="ineq", value=2)
    execute_algo(opt_problem=problem, algo_name="COBYQA")


def test_stop_crit_n():
    """Test that the ``stop_crit_n`` option is taken into account by default.

    This ensures that ``COBYQA`` performs at least nb_points+1 points before checking
    the convergence criteria.
    """
    design_space = DesignSpace()
    nb_var = 10
    design_space.add_variable("x", l_b=0, u_b=1, value=0.5, size=nb_var)
    problem = OptimizationProblem(design_space)
    problem.objective = MDOFunction(lambda x: sum(x), "my_func")
    execute_algo(opt_problem=problem, algo_name="COBYQA")

    assert problem.current_iter > 2 * nb_var + 2


def test_radius_init(problem):
    """Test that the ``radius_init`` option is cascaded properly."""
    execute_algo(opt_problem=problem, algo_name="COBYQA", max_iter=2, radius_init=0.1)
    assert problem.database.get_x_vect(2)[0] == 0.8


def test_radius_final(problem):
    """Test the ``radius_final`` option is cascaded properly."""
    result_1 = execute_algo(opt_problem=problem, algo_name="COBYQA", radius_final=1e-1)
    problem.reset()
    result_2 = execute_algo(opt_problem=problem, algo_name="COBYQA", radius_final=1e-2)
    assert result_2.n_obj_call > result_1.n_obj_call
